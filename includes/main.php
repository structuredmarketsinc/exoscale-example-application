<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Plugin main class that will control the whole skeleton and common functions
 *
 * PHP version 5
 *
 * @category   Main
 * @package    Microsoft exoscale API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class EXOSCALE
{ 
     
    //Plugin starting point. Will call appropriate actions
    public function __construct() {

        add_action( 'plugins_loaded', array( $this, 'exoscale_init' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'exoscale_enqueue_scripts' ), 10 );
        add_action( 'admin_enqueue_scripts', array( $this, 'exoscale_enqueue_scripts' ), 10 );
    }

    //Plugin initialization
    public function exoscale_init() {

        do_action('exoscale_before_init');
        
        global $exoscale_options;
        
        $exoscale_options = get_option( 'exoscale_settings' );
        load_plugin_textdomain( 'exoscale', FALSE, EXOSCALE_LANG_DIR );        
        
        if(is_admin()){            
            require_once EXOSCALE_PLUGIN_PATH.'includes/admin.php';            
        }

        do_action('exoscale_after_init');
    }
    
    //Function will add CSS and JS files
    public function exoscale_enqueue_scripts() {
        
        do_action('exoscale_before_enqueue_scripts');
        
        wp_enqueue_script( 'jquery' );
        wp_register_script( 'exoscale_jtable_js', EXOSCALE_PLUGIN_URL. "assets/js/jtable/jquery.jtable.js", array( 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-position', 'jquery-ui-accordion', 'jquery-ui-autocomplete', 'jquery-ui-button', 'jquery-ui-datepicker', 'jquery-ui-dialog', 'jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-effects-blind', 'jquery-effects-bounce', 'jquery-effects-clip', 'jquery-effects-drop', 'jquery-effects-explode', 'jquery-effects-fade', 'jquery-effects-fold', 'jquery-effects-highlight', 'jquery-effects-pulsate', 'jquery-effects-scale', 'jquery-effects-shake', 'jquery-effects-slide', 'jquery-effects-transfer', 'jquery-ui-menu', 'jquery-ui-progressbar', 'jquery-ui-resizable', 'jquery-ui-selectable', 'jquery-ui-slider', 'jquery-ui-sortable', 'jquery-ui-spinner', 'jquery-ui-tabs', 'jquery-ui-tooltip' ), time() );
        wp_register_style( 'exoscale_jtable_css', EXOSCALE_PLUGIN_URL. "assets/js/jtable/themes/lightcolor/gray/jtable.css" );        
        wp_enqueue_script( 'exoscale_js', EXOSCALE_PLUGIN_URL.'assets/js/javascript.js', array( 'jquery' ), time() );
        wp_enqueue_style( 'exoscale_css', EXOSCALE_PLUGIN_URL.'assets/css/style.css', array(), time() );
        
        wp_localize_script( 'exoscale_js', 'exoscale_data', array(
                            'ajaxurl' => admin_url( 'admin-ajax.php' )
                            ));
        
        do_action('exoscale_after_enqueue_scripts');
    }
    
    /*
     * Function to get all Offerings
     * 
     * @param $params array of optional parameters
     * 
     * @return $result mixed data of Offerings || Error message
     */
    public function exoscale_get_offerings( $params = array() ) {
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "listServiceOfferings", $params );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to get all Zones
     * 
     * @param $params array of optional parameters
     * 
     * @return $result mixed data of Zones || Error message
     */
    public function exoscale_get_zones( $params = array() ) {
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "listZones", $params );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to get all Templates
     * 
     * @param $params array of optional parameters
     * 
     * @return $result mixed data of Templates || Error message
     */
    public function exoscale_get_templates( $params = array() ) {
        
        global $exoscale_error;
        
        if( empty( $params ) || !isset( $params ) || !isset( $params['templatefilter'] ) ) {
            return __( 'templatefilter Parameter is required', 'exoscale' );
        }
        
        if( $params['templatefilter'] == 'all' ) {
            
            //Self Templates
            $params['templatefilter'] = "self";
            $self_templates = $this->exoscale_do_api( "listTemplates", $params );

            //Featured Templates
            $data['templatefilter'] = "featured";
            $featured_templates = $this->exoscale_do_api( "listTemplates", $params );

            //Comunity Templates
            $data['templatefilter'] = "community";
            $community_templates = $this->exoscale_do_api( "listTemplates", $params );

            //Merge all templates
            if( $featured_templates || $community_templates || $self_templates ) {
                $result = new StdClass();
                $result->count      = 0;
                $result->template   = array();
                if( $self_templates && isset($self_templates->count) ) {
                    $result->count      = $result->count+$self_templates->count;
                    $result->template   = array_merge( $result->template, $self_templates->template );
                }

                if( $featured_templates && isset($featured_templates->count) ) {
                    $result->count      = $result->count+$featured_templates->count;
                    $result->template   = array_merge( $result->template, $featured_templates->template );
                }

                if( $community_templates && isset($community_templates->count) ) {
                    $result->count      = $result->count+$community_templates->count;
                    $result->template   = array_merge( $result->template, $community_templates->template );
                }
            }

        }else if( !empty( $params['templatefilter'] ) ){
            $result = $this->exoscale_do_api( "listTemplates", $params );
        }
        
        if( !$result ) {
            return $exoscale_error;            
        }
            
        return $result;
    }
    
    /*
     * Function to get all SSH KeyPairs
     * 
     * @param $params array of optional parameters
     * 
     * @return $result mixed data of SSH KeyPairs || Error message
     */
    public function exoscale_list_ssh_keypairs( $params = array() ) {
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "listSSHKeyPairs", $params );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to get all VMs
     * 
     * @param $params array of optional parameters
     * 
     * @return $result mixed data of VMs || Error message
     */
    public function exoscale_list_vms( $params = array() ) {
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "listVirtualMachines", $params );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to create new SSH KeyPair
     * 
     * @return $result mixed data of API call || Error message
     */
    public function exoscale_create_ssh_keypair( $name = "" ) {
        
        if( empty($name) ) {
            return __( 'Required data not provided to create the SSH KeyPair', 'exoscale' );
        }
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "createSSHKeyPair", array( "name" => $name ) );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to Deploy Virtual Machine
     * 
     * @param $data array required parameters to deploy the VM
     * 
     * @return $result mixed data of API call || Error message
     */
    public function exoscale_deploy_vm( $data = array() ) {
        
        if( empty($data) || !isset($data['serviceofferingid']) || !isset($data['templateid']) || !isset($data['zoneid']) ) {
            return __( 'Required data not provided to deploy the VM', 'exoscale' );
        }
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "deployVirtualMachine", $data );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to reeboot a VM
     * 
     * @param $vm_id string VM id
     * 
     * @return $result mixed data of API call || Error message
     */
    public function exoscale_reboot_vm( $vm_id = "" ) {
        
        if( empty($vm_id) ) {
            return __( 'Required data not provided to reboot the VM', 'exoscale' );
        }
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "rebootVirtualMachine", array( "id" => $vm_id ) );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to start a VM
     * 
     * @param $vm_id string VM id
     * 
     * @return $result mixed data of API call || Error message
     */
    public function exoscale_start_vm( $vm_id = "" ) {
        
        if( empty($vm_id) ) {
            return __( 'Required data not provided to reboot the VM', 'exoscale' );
        }
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "startVirtualMachine", array( "id" => $vm_id ) );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to stop a VM
     * 
     * @param $vm_id string VM id
     * 
     * @return $result mixed data of API call || Error message
     */
    public function exoscale_stop_vm( $vm_id = "" ) {
        
        if( empty($vm_id) ) {
            return __( 'Required data not provided to reboot the VM', 'exoscale' );
        }
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "stopVirtualMachine", array( "id" => $vm_id ) );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to delete a VM
     * 
     * @param $vm_id string VM id
     * 
     * @return $result mixed data of API call || Error message
     */
    public function exoscale_destroy_vm( $vm_id = "" ) {
        
        if( empty($vm_id) ) {
            return __( 'Required data not provided to delete the VM', 'exoscale' );
        }
        
        global $exoscale_error;
        
        $result = $this->exoscale_do_api( "destroyVirtualMachine", array( "id" => $vm_id ) );
        
        if( !$result ) {
            return $exoscale_error;            
        }
        
        return $result;
    }
    
    /*
     * Function to make cURL API calls
     * 
     * @param $command string refers to the command we wish to execute
     * @param $data array of parameters for the given command
     *  
     * @return $response mixed json object response or null 
     */
    public function exoscale_do_api( $command = "", $data = array(), $method = "GET" ) {
        
        global $exoscale_options, $exoscale_error;
        
        //Set error message to null
        $exoscale_error = null;
        
        if( empty($command)  ) {
            $exoscale_error = __( 'No command provided to execute', 'exoscale' );
            return false;
        }
        
        //Make sure data is lower case and alphabatically sorted
        $params = "";
        $data['command']    = $command;
        $data['apikey']     = $exoscale_options['api_key'];
        ksort($data);
        foreach( $data as $k => $v ) {
            $params.= strtolower($k)."=".strtolower(rawurlencode($v))."&";
        }
        
        //Signing API request
        $params     = utf8_encode(rtrim($params,"&"));
        $signature  = rawurlencode(utf8_decode( base64_encode( hash_hmac("sha1", $params, utf8_encode($exoscale_options['api_secret']), true ) )));
        
        //Make Querystring
        $querystring = http_build_query($data)."&signature=".$signature;
        
        $url =  rtrim($exoscale_options['api_end_point'],"/")."?".$querystring;
        
        //Do curl
        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, $method);
        
        $response = curl_exec($cURLConnection);
        curl_close($cURLConnection);            
        $response = current(json_decode($response));
        
        if( isset($response->errorcode)  ) {
            print_r($response);
            $exoscale_error = $response->errorcode.": ".$response->errortext;
            return false;
        }elseif( empty($response) ){
            $exoscale_error = __( 'No data found!', 'exoscale' );
            $response = false;
        }
        
        return $response;
    }
    
    /*
     * load front end or admin end html template
     * 
     * @param $template template name
     * @param $for either for front end and back end
     * @param $attr array of all parameters that we will pass to the template
     * 
     * return html of the template
     */
    public function exoscale_load_template( $template='', $for='front', $attr=array() ) {
        
        global $exoscale_options;
        
        do_action( 'exoscale_before_load_template', $template, $for, $attr );
        
        $template = apply_filters( 'exoscale_template_to_load', $template, $for, $attr );
        $attr = apply_filters( 'exoscale_template_variables', $attr, $template, $for );
        
        if( empty($template) ) {
            return '';
        }
        if( is_array($attr) ) {
            extract($attr);
        }
        
        $html = '';
        $html = apply_filters( 'exoscale_before_template_html', $html, $template, $for, $attr );
        
        ob_start();
        require EXOSCALE_PLUGIN_PATH.'templates/'.$for.'/'.$template.'.php';
        $html = ob_get_contents();
        ob_end_clean();  
        
        do_action( 'exoscale_after_load_template', $template, $for, $attr, $html );
        
        $html = apply_filters( 'exoscale_after_template_html', $html, $template, $for, $attr );
        
        return $html;
    }
    
    /*
     * load admin end messages/errors/warnings template
     * 
     * @param $message message string
     * @param $type either it is message? or error ? or warning?
     * 
     * return html of the template
     */
    public function exoscale_get_message_html( $message, $type = 'message' ) {
        
        global $exoscale_options;
        
        do_action( 'exoscale_before_get_message_html', $message, $type );
        $message = apply_filters( 'exoscale_message_text', $message, $type );
        $type = apply_filters( 'exoscale_message_type', $type, $message );
        
        $html = '';
        
        $html = apply_filters( 'exoscale_before_message_html', $html, $message, $type );
        
        $attr = array( 'message' => $message, 'type' => $type );
        
        $html = $this->exoscale_load_template( $type, 'common', $attr );
        
        do_action( 'exoscale_after_get_message_html', $message, $type );
        $html = apply_filters( 'exoscale_after_message_html', $html, $message, $type );
        
        return $html;
    }
    
    /*
     * make a variable snaitize and 
     * handel quotes double quotes and other characters 
     * 
     * @param $variable
     * 
     * return snaitizeed variable 
     */
    public function make_safe( $variable ) {

        $variable = sanitize_text_field($variable);
        $variable = esc_html($variable);
        
        return $variable;
    }
    
    /*
     * Function to sort an array
     */
    public function array_sort($array, $on, $order='ASC'){

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case 'ASC':
                    asort($sortable_array);
                    break;
                case 'DESC':
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}

$exoscale = new EXOSCALE();