<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Class that will hold functionality for admin side
 *
 * PHP version 5
 *
 * @category   Admin Side Code
 * @package    Microsoft exoscale API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class EXOSCALE_Admin extends EXOSCALE
{
    //Admin side starting point. Will call appropriate admin side hooks
    public function __construct() {
        
        do_action('exoscale_before_admin', $this );
        //All admin side code will go here
        
        add_action( 'admin_menu', array( $this, 'exoscale_admin_menus' ) );    
        //Action hook to show instances
        add_action( 'wp_ajax_exoscale_get_all_vms', array( $this, 'exoscale_get_all_vms') );
        //Action hook to save VMs
        add_action( 'wp_ajax_exoscale_save_vm', array( $this, 'exoscale_save_vm') );
        
        //Action hook to get templates options html
        add_action( 'wp_ajax_exoscale_get_all_templates_html', array( $this, 'exoscale_get_all_templates_html') );
        
        //Action hook to save new field data
        add_action( 'wp_ajax_exoscale_submit_new_field', array( $this, 'exoscale_submit_new_field') );
        
        do_action('exoscale_after_admin', $this );            
    }
    
    /*
     * Function to show admin menu
     */
    public function exoscale_admin_menus(){
        
        add_menu_page( EXOSCALE_PLUGIN_NAME, EXOSCALE_PLUGIN_NAME, 'manage_options', 'exoscale_settings', array( $this, 'exoscale_settings' ) );
        add_submenu_page( 'exoscale_settings', EXOSCALE_PLUGIN_NAME.' Settings', 'Settings', 'manage_options', 'exoscale_settings', array( $this, 'exoscale_settings' ) );
        add_submenu_page( 'exoscale_settings', EXOSCALE_PLUGIN_NAME.' Virtual Machines', 'Virtual Machines', 'manage_options', 'exoscale_vm', array( $this, 'exoscale_vm' ) );
    }    
    
    /*
     * Settings page of the plugin
     */
    public function exoscale_settings() {
        
        global $exoscale_options, $exoscale_lang;
        
        do_action('exoscale_before_settings', $this, $exoscale_options );
        $message = '';
        if( isset($_POST['btnsave']) && $_POST['btnsave'] != "" ) {            
            $exclude = array('btnsave');
            $exoscale_options = array();            
            foreach( $_POST as $k => $v ) {
                if( !in_array( $k, $exclude )) {
                    if(!is_array($v)) {
                        $val = $this->make_safe($v);
                    }else{
                        $val = $v;
                    }
                    $exoscale_options[$k] = $val;
                }
            }            
            update_option( 'exoscale_settings', $exoscale_options );
            
            //Load success message template
            $message = $this->exoscale_get_message_html( __( 'Settings saved successfully!', 'exoscale' ), 'message' );
        }
        
        $attr = $exoscale_options;
        $attr['message'] = $message;
        //Load settings page template
        $html = $this->exoscale_load_template( "settings", "admin", $attr );
        
        do_action('exoscale_after_settings', $this, $exoscale_options );
        
        echo $html;
    }
    
    /*
     * Ajax function to get all regions
     */
    public function exoscale_get_keypair_html( $return = false ) {
        
        $message = $html = "";
        $error = false;
        $keypairs = $this->exoscale_list_ssh_keypairs();
        if( !is_object($keypairs ) ) {
            $error = true;
            $message = __( $keypairs, 'exoscale' );
        }
        
        if( !$error ) {            
            $html = '<option value="">'.__( 'Select SSH Key Pairs', 'exoscale' ).'</option>';
            if(is_array($keypairs->sshkeypair)) {
                foreach( $keypairs->sshkeypair as $keypair ) {
                    $html.= '<option value="'.$keypair->name.'">'.$keypair->name.'</option>';                                        
                }
            }
            $html.='<option value="" data-create="yes">'.__( 'Create New SSH Key', 'exoscale' ).'</option>';
        }
        if( $return ) {
            return $html;
        }
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Ajax function to get all templates
     */
    public function exoscale_get_all_templates_html( $return=false ) {
        
        $message = $html = "";
        $error = false;
        $zoneid = filter_input( INPUT_POST, 'zoneid' );
        if( empty($zoneid) ) {
            $error = true;
            $message = __( 'No zone provided', 'exoscale' );
        }
        
        if( !$error ) {
            $html = '<option value="">'.__( 'Select Template', 'exoscale' ).'</option>';
            $result = $this->exoscale_get_templates( array( "zoneid" => $zoneid, "templatefilter" => "all" ) );
            if( is_object($result ) && is_array($result->template) ) {
                foreach ( $result->template as $template ) {
                    $html.='<option value="'.$template->id.'">'.$template->displaytext.'</option>';
                }
            }else{
                $error  = true;
                $message= $result;
            }
        }
        if( $return ) {
            return $html;
        }
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Ajax function to save new value for object
     */
    public function exoscale_submit_new_field() {
        
        $message = $html = "";
        
        $val    = filter_input( INPUT_POST, 'val' );
        $task   = filter_input( INPUT_POST, 'task' );
        
        $error = false;
        if( empty($val) ) {
            $error  = true;
            $message = __( 'You must provide some data', 'exoscale' );
        }
        
        if( empty($task) ) {
            $error  = true;
            $message = __( 'You must provide some task to do', 'exoscale' );
        }
        
        if( !$error ) {
            if( $task == 'keypair' ) {
                $result = $this->exoscale_create_ssh_keypair($val);                
                if( !is_object($result) ) {
                    $error  = true;
                    $message= $result;
                }else{
                    $html = $this->exoscale_get_keypair_html( true );
                }
            }            
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        
        wp_send_json($response);
    }
    
    /*
     * Ajax function to save and Launch the VM
     */
    public function exoscale_save_vm() {
        
        $message = '';
        $data = $_POST;
        $error = false;
        unset($data['btnsave']);
        $result = $this->exoscale_deploy_vm( $data );
        if(is_object($result) ) {
            $message = __( 'Virtual Machine saved successfully!', 'exoscale' );            
        }else{
            $message = $result;
        }
        $response = array( 'error' => $error, 'message' => $message );
        wp_send_json($response);
    }
    
    /*
     * Admin side page to show instances
     */
    public function exoscale_vm() {
        
        $template = $html = $message = '';
        $action = filter_input( INPUT_GET, 'action' );
        $id     = filter_input( INPUT_GET, 'id' );
        
        if( $action == 'edit' && is_numeric( $id ) ) {
            //do edit
        }elseif( $action == 'add' ) {
            if( isset($_POST['btnsave']) && $_POST['btnsave'] != "" ) {  
                $data = $_POST;
                unset($data['btnsave']);
                if($this->exoscale_add_record( "exoscale_vm", $data )) {
                    $message = $this->exoscale_get_message_html( __( 'Virtual Machine saved successfully!', 'exoscale' ), 'message' );
                }else{
                    $message = $this->exoscale_get_message_html( __( 'There is some error to save the Virtual Machine', 'exoscale' ), 'error' ); 
                }
            }
            $serviceofferings   = $this->exoscale_get_offerings();
            $zones              = $this->exoscale_get_zones();
            $keypairs           = $this->exoscale_list_ssh_keypairs();
            $attr = array();
            $attr['serviceofferings']   = $serviceofferings;
            $attr['zones']              = $zones;
            $attr['keypairs']           = $keypairs;
            $template = "add_vm";  
        }else{
            if( $action == 'del' && !empty( $id ) ){
                $result = $this->exoscale_destroy_vm( $id );                
                if(is_object($result)) {
                    $message = $this->exoscale_get_message_html( __( 'VM Deleted successfully!', 'exoscale' ), 'message' ); 
                }else{
                    $message = $this->exoscale_get_message_html( __( $result, 'exoscale' ), 'error' ); 
                }
            }elseif( $action == 'start' && !empty( $id ) ){
                $result = $this->exoscale_start_vm( $id );
                if(is_object($result)) {
                    $message = $this->exoscale_get_message_html( __( 'VM Started successfully!', 'exoscale' ), 'message' ); 
                }else{
                    $message = $this->exoscale_get_message_html( __( $result, 'exoscale' ), 'error' ); 
                }
            }elseif( $action == 'stop' && !empty( $id ) ){
                $result = $this->exoscale_stop_vm( $id );
                if(is_object($result)) {
                    $message = $this->exoscale_get_message_html( __( 'VM Stoped successfully!', 'exoscale' ), 'message' ); 
                }else{
                    $message = $this->exoscale_get_message_html( __( $result, 'exoscale' ), 'error' ); 
                }
            }elseif( $action == 'reboot' && !empty( $id ) ){
                $result = $this->exoscale_reboot_vm( $id );
                if(is_object($result)) {
                    $message = $this->exoscale_get_message_html( __( 'VM Rebooted successfully!', 'exoscale' ), 'message' ); 
                }else{
                    $message = $this->exoscale_get_message_html( __( $result, 'exoscale' ), 'error' ); 
                }
            }elseif ($action == 'del' && $_REQUEST['ids'] != "" ){
                $ids = explode(",", $_REQUEST['ids']);
                if(is_array($ids)) {
                    foreach($ids as $id) {
                        if(!empty($id)) {
                            $result = $this->exoscale_destroy_vm( $id );
                            if(is_object($result)) {
                                $message = $this->exoscale_get_message_html( __( 'VM Deleted successfully!', 'exoscale' ), 'message' ); 
                            }else{
                                $message = $this->exoscale_get_message_html( __( $result, 'exoscale' ), 'error' ); 
                            }
                        }
                    }
                }
            }
            wp_enqueue_script( 'exoscale_jtable_js' );
            wp_enqueue_style( 'exoscale_jtable_css' );
            $attr = array();
            $attr = $_REQUEST;
            $template = "vms";            
        }
        if( !empty($template) ) {
            $attr['message'] = $message;
            $html = $this->exoscale_load_template( $template, "admin", $attr );
        }
        echo $html;
    }
    
    /*
     * Ajax callback function to load instances
     */
    public function exoscale_get_all_vms() {
        
        $sorting    = filter_input( INPUT_GET, 'jtSorting' );
        $start      = filter_input( INPUT_GET, 'jtStartIndex' );
        $page_size  = filter_input( INPUT_GET, 'jtPageSize' );
        $end        = $start+$page_size;
        $rows       = $final_rows = array();
        $recordCount= 0;
        
        $records = $this->exoscale_list_vms(); 
        if( is_object($records) && isset($records->virtualmachine) && is_array($records->virtualmachine) ) {
            $records = $records->virtualmachine;
            
            $recordCount = sizeof($records);
            if( $sorting != "" ) {
                $sorting_arr = explode(" ",$sorting);
                $orderby = trim($sorting_arr[0]);
                $order = trim($sorting_arr[1]);
            }
            
            $index = 0;
            foreach ( $records as $row ) {
                $state = strtolower($row->state);
                $actions = '';
                if( $state == 'starting' || $state == 'running' ) {
                    $actions.='<td><a class="exoscale_actions '.$state.'" title="Stop" href="admin.php?page=exoscale_vm&action=stop&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to stop it?\');">'.__( 'Stop', 'exoscale' ).'</a></td>';
                    $actions.='<td><a class="exoscale_actions terminate" title="Reboot" href="admin.php?page=exoscale_vm&action=reboot&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to reboot it?\');">'.__( 'Reboot', 'exoscale' ).'</a></td>';
                }elseif( $state != 'creating' && $state != 'stopping' ) {
                    $actions.='<td><a class="exoscale_actions '.$state.'" title="Start" href="admin.php?page=exoscale_vm&action=start&id='.$row->id.'">'.__( 'Start', 'exoscale' ).'</a></td>';
                }
                $actions.='<td><a class="exoscale_actions delete" title="Delete" href="admin.php?page=exoscale_vm&action=del&id='.$row->id.'&state='.$state.'" onclick="javascript: return confirm(\'Are you sure you want to terminate it?\');">'.__( 'Delete', 'exoscale' ).'</a></td>';

                $record = array();
                $record['Action']       = '<table class="exoscale_actions"><tr>'.$actions.'</tr></table>';
                $record['ID']           = $row->id;
                $record['Name']         = $row->displayname;
                $record['Zone']         = $row->zonename;                
                $record['Template']     = $row->templatename;                
                $record['State']        = str_replace( "_", " ", strtoupper($state) );
                $rows[] = $record;

            }

            $rows = $this->array_sort($rows, $orderby,$order);
            foreach ( $rows as $row ) {
                if($index >= $start && $index <= $end) {
                    $final_rows[] = $row;
                }
                $index++;
            }
        }
        
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $final_rows;
        print json_encode($jTableResult);
        exit();
    }
}

$exoscale_admin = new EXOSCALE_Admin();