jQuery(document).ready(function($){  
    
    //If subscription select box value changed
    if( $("#frm_exoscale_add #zoneid").length ) {
        $(document).on("change", '#frm_exoscale_add #zoneid', function(e) { 
            //Get All Templates
            exoscale_get_all_templates_html($(this));
        });
    }
    
    if( $("#frm_exoscale_add select").length ) {
        $(document).on("change", '#frm_exoscale_add select', function(e) { 
            //Load create new text field
            exoscale_create_new_field($(this));              
        });
    }
    
    if( $("#frm_exoscale_add").length ) {
        $(document).on("submit", '#frm_exoscale_add', function(e) { 
            e.preventDefault();
            exoscale_save_vm($(this));
        });
    }
});

/*
 * Save VM
 */
function exoscale_save_vm( form ) {
    
    var $ = jQuery;
    show_ajax_loader(form.find("#btnsave"));
    form.find("#btnsave").prop("disabled",true);

    $.ajax({
            type: "POST",
            url: exoscale_data.ajaxurl+"?action=exoscale_save_vm",
            data: form.serialize(),// serializes the form's elements.
            success: function(response) {                        
                if(response.error) {
                    alert(response.message);                            
                }else{ 
                    alert(response.message);
                    window.location.reload();
                }
                hide_ajax_loader(form.find("#btnsave"));
                form.find("#btnsave").prop("disabled",false);                        
            },
            error: function (xhr, status, error) {   
                hide_ajax_loader(form.find("#btnsave"));
                form.find("#btnsave").prop("disabled",false);
                alert("There is some error to connect with server. Please try again.");
            }
    });
}

/*
 * Create new field
 */
function exoscale_create_new_field( obj ) {
    
    var $ = jQuery;
    var obj_id = $(obj).attr("id");
    if( $(obj.find("option:selected")).attr("data-create") == 'yes' ) {        
        if(!$("#new_"+obj_id).length) {
            obj.after('<input class="new_" type="text" name="new_'+obj_id+'" id="new_'+obj_id+'" /><input type="button" name="new_btn_'+obj_id+'" id="new_btn_'+obj_id+'" value="Create" onclick="javascript: exoscale_submit_new_field(\''+obj_id+'\')" />');
        }else{
            exoscale_remove_new_field( obj_id );
        }
    }else{
        exoscale_remove_new_field( obj_id );
    }
}

/*
 * Remove new field
 */
function exoscale_remove_new_field( obj_id ) {
    
    var $ = jQuery;
    if($("#new_"+obj_id).length) {
        $("#new_"+obj_id).remove();
        $("#new_btn_"+obj_id).remove();
    }
}

/*
 * Submit new field
 */
function exoscale_submit_new_field( obj_id ) {
    
    var $ = jQuery;
    var obj = $("#new_"+obj_id);
    var val = obj.val();
    var patt = /^[-\w\._\(\)]+$/;
    if( !patt.test(val) ) {
        alert("Please provide valid text, Can include alphanumeric, underscore, parentheses, hyphen, period (except at end).");
        return;
    }
    show_ajax_loader(obj.next());
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: exoscale_data.ajaxurl,
        data: {
                action: 'exoscale_submit_new_field',
                task: obj_id,
                val:val,
                subscription: $("#frm_exoscale_add #subscription").val(),
                region:$("#frm_exoscale_add #region").val(),
                group:$("#frm_exoscale_add #group").val()
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
                $("#"+obj_id).prop("selectedIndex", 0);
            }else{ 
                hide_ajax_loader(obj.next());
                $("#"+obj_id).html(response.html);  
                var selectedval = val;
                $("#"+obj_id+"  option").each(function(){
                    if( $(this).text() == val ) {
                        selectedval = $(this).val();
                    }
                });
                $("#"+obj_id).val(selectedval);
                $("#"+obj_id).trigger("change");
            }            
        },
        error: function (xhr, status, error) {            
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
       hide_ajax_loader(obj.next());  
       exoscale_remove_new_field( obj_id );
    });
}

/*
 * Function to hide all rows except the current
 * 
 * @param string current
 * @returns null
 */
function exoscale_hide_next_rows( current ) {
    
    var $   = jQuery;
    var list= [ "serviceofferingid", "zoneid", "templateid" ];
    var hide= false;
    for( var i=0; i<list.length; i++ ) {
        if( list[i] == current ) {
            hide = true;
        }
        if( hide ) {
            $(".show_if_"+list[i]).hide();
        }
    }
}

/*
 * Show all templates select box
 */
function exoscale_get_all_templates_html( obj ) {
    
    var $ = jQuery;
    exoscale_hide_next_rows("zoneid");
    if( $(obj).val() == "" ) {
        return;
    }
    show_ajax_loader(obj);    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: exoscale_data.ajaxurl,
        data: {
                action: 'exoscale_get_all_templates_html',
                zoneid: $("#frm_exoscale_add #zoneid").val()
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $("#templateid").html(response.html);
                $(".show_if_zoneid").show();
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
       hide_ajax_loader(obj);             
    });
}

//Show Ajax loader
function show_ajax_loader( obj ) {
    
    if(!obj.next('.exoscale_ajax_loader').length) {
        obj.after('<div class="exoscale_ajax_loader"></div>');
        obj.next('.exoscale_ajax_loader').show();
    }
}

//Remove Ajax loader
function hide_ajax_loader( obj ) {
    
    if(obj.next('.exoscale_ajax_loader').length) {
        obj.next('.exoscale_ajax_loader').remove();
    }
}

//Delete multiple/all objects on listing page
function exoscale_delete_all_selected(obj) {
    var $ = jQuery;
    var page = $(obj).attr("data-page");
    if(confirm('Are you sure you want to delete all selected items?')) {
        var ids = "";
        $(".exoscale_select_item").each(function(){
            if($(this).is(":checked")){
                if(ids=="") {
                    ids = $(this).val();
                }else{
                    ids = ids+","+$(this).val();
                }
            }
        });
        if(ids=="") {
            alert("You must select some items to delete.");
        }else{
            window.location.href="admin.php?page="+page+"&action=del&ids="+ids;
        }
    }
    return false;
}

//Select all objects on listing page
function exoscale_select_all(obj) {
    var $ = jQuery;
    if($(obj).is(":checked")) {
        $(".exoscale_select_item").prop("checked",true);
    }else{
        $(".exoscale_select_item").prop("checked",false);
    }
}