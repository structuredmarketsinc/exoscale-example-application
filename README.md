# README #

Contains code for a plugin that creates VMs on the EXOSCALE cloud.

### What is this repository for? ###

This project contains code for a plugin that creates VMs on the EXOSCALE cloud.

### How do I get set up? ###

To install:
1. Zip up folder
2. Upload to your wordpress server using the PLUGINS screen.
3. Activate.

You can create servers, stop them, reboot and delete them.

### Who do I talk to? ###

This plugin was created in order for us to understand how the EXOSCALE API works.  There is no support for it.
However, you can see where this project lead by going to the https://wpclouddeploy.com website.
