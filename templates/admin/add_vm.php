<?php if ( $message!="" ) { echo $message; }?>
<div class="wrap">
<h2><?php echo __( 'Add Virtual Machine', 'exoscale' );?></h2>
<table class="wp-list-table widefat fixed" cellspacing="0">
	<thead>
        <tr>
            <th scope="col" class="manage-column" style=""><?php echo __( 'Virtual Machine Detail', 'exoscale' );?></th>
        </tr>
	</thead>
	<tbody id="the-list">
        <tr>
            <td>
            	<form method="post" name="frm_exoscale" id="frm_exoscale_add" class="frm_exoscale" action="?page=exoscale_vm&action=add" enctype="multipart/form-data">                    
                <table width="100%">
                    <tr>
                    	<td width="180"><?php echo __( 'Exoscale Service Offering', 'exoscale' );?></td>
                        <td>
                            <select name="serviceofferingid" id="serviceofferingid">
                                <option value=""><?php echo __( 'Select Service Offering', 'exoscale' );?></option>
                                <?php
                                if(is_array($serviceofferings->serviceoffering)) {
                                    foreach( $serviceofferings->serviceoffering as $serviceoffering ) {
                                        echo '<option value="'.$serviceoffering->id.'">'.$serviceoffering->displaytext.'</option>';                                        
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td><?php echo __( 'Exoscale Zones', 'exoscale' );?></td>
                        <td>
                            <select name="zoneid" id="zoneid">
                                <option value=""><?php echo __( 'Select Zones', 'exoscale' );?></option>   
                                <?php
                                if(is_array($zones->zone)) {
                                    foreach( $zones->zone as $zone ) {
                                        echo '<option value="'.$zone->id.'">'.$zone->name.'</option>';                                        
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="hidden show_if_zoneid">
                    	<td><?php echo __( 'Exoscale Templates', 'exoscale' );?></td>
                        <td>
                            <select name="templateid" id="templateid">
                                <option value=""><?php echo __( 'Select Template', 'exoscale' );?></option>                                
                            </select>                            
                        </td>
                    </tr>  
                    <tr class="hidden show_if_zoneid">
                    	<td><?php echo __( 'Exoscale VM Name', 'exoscale' );?></td>
                        <td>
                            <input type="text" name="name" id="name" />
                        </td>
                    </tr>
                    <tr class="hidden show_if_zoneid">
                    	<td><?php echo __( 'Exoscale SSH KeyPairs', 'exoscale' );?></td>
                        <td>
                            <select name="keypair" id="keypair">
                                <option value=""><?php echo __( 'Select SSH Key Pairs', 'exoscale' );?></option>    
                                <?php
                                if(is_array($keypairs->sshkeypair)) {
                                    foreach( $keypairs->sshkeypair as $keypair ) {
                                        echo '<option value="'.$keypair->name.'">'.$keypair->name.'</option>';                                        
                                    }
                                }
                                ?>
                                <option value="" data-create="yes"><?php echo __( 'Create New SSH Key', 'exoscale' );?></option>
                            </select>                            
                        </td>
                    </tr>
                    <tr class="hidden show_if_zoneid">
                        <td></td>
                        <td><input type="submit" name="btnsave" id="btnsave" value="<?php echo __( 'Add Virtual Machine', 'exoscale' );?>" class="button button-primary">
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
     </tbody>
</table>
</div>