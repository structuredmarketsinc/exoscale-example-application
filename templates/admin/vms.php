<?php if ( $message!="" ) { echo $message; }?>
<style>
    .exoscale_view_icon{margin-left:10px;width: 32px;height: 16px;font-size: 0px;background:url("<?php echo EXOSCALE_PLUGIN_URL;?>assets/images/view.png") no-repeat;display: inline-block;}
    .exoscale_edit_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo EXOSCALE_PLUGIN_URL;?>assets/images/edit.png") no-repeat;display: inline-block;}
    .exoscale_del_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo EXOSCALE_PLUGIN_URL;?>assets/images/del.png") no-repeat;display: inline-block;}
    .exoscale_pdf_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo EXOSCALE_PLUGIN_URL;?>assets/images/pdf.png") no-repeat;display: inline-block;}
</style>
<div class="wrap">
    <br>
        <h1><?php echo __( 'Virtual Machines', 'exoscale' );?> <a href="?page=exoscale_vm&action=add" class="page-title-action">Add New</a></h1>
    <br>
    <div id="FieldsTableContainer" style="width: 100%;"></div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#FieldsTableContainer').jtable({
                title: 'Manage All Virtual Machines',
                paging: true,
                pageSize: 20,
                sorting: true,
                defaultSorting: 'ID ASC',
                actions: {
                    listAction: ajaxurl+'?action=exoscale_get_all_vms'
                },
                fields: {
                        Action: {
                            title: 'Actions',
                            width: '20px',
                            sorting: false
                        },
                        ID: {
                            title: 'ID',
                            key: true,
                            create: false,
                            edit: false,
                            list: true,
                            width: '20%'
                        },
                        State: {
                            title: 'Current State',
                            width: '15%'
                        },
                        Name: {
                            title: 'Name',
                            width: '20%'
                        },
                        Zone: {
                            title: 'Zone',
                            width: '15%'
                        },
                        Template: {
                            title: 'Template',
                            width: '20%'
                        }
                }
            });
            //Load person list from server
            jQuery('#FieldsTableContainer').jtable('load');
            window.history.pushState("", "", '?page=exoscale_vm');
        });
    </script>            
</div>