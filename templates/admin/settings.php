<?php if ( $message!="" ) { echo $message; }?>
<div class="wrap">
<h2><?php echo __( 'Settings', 'exoscale' );?></h2>
<table class="wp-list-table widefat fixed" cellspacing="0">
	<thead>
        <tr>
            <th scope="col" class="manage-column" style=""><?php echo __( 'Settings', 'exoscale' );?></th>
        </tr>
	</thead>
	<tbody id="the-list">
        <tr>
            <td>
            	<form method="post" name="frm_exoscale" id="frm_exoscale" class="frm_exoscale" action="?page=exoscale_settings" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                    	<td width="180"><?php echo __( 'Exoscale API Key', 'exoscale' );?></td>
                        <td>
                            <input type="text" name="api_key" id="api_key" value="<?php echo isset($api_key)?$api_key:"";?>" />
                        </td>
                    </tr>
                    <tr>
                    	<td><?php echo __( 'Exoscale API Secret', 'exoscale' );?></td>
                        <td>
                            <input type="text" name="api_secret" id="api_secret" value="<?php echo isset($api_secret)?$api_secret:"";?>" />
                        </td>
                    </tr>
                    <tr>
                    	<td><?php echo __( 'API End Point', 'exoscale' );?></td>
                        <td>
                            <input type="text" name="api_end_point" id="api_end_point" value="<?php echo isset($api_end_point)?$api_end_point:"";?>" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="btnsave" id="btnsave" value="<?php echo __( 'Update', 'exoscale' );?>" class="button button-primary">
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
     </tbody>
</table>
</div>