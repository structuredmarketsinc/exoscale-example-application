<?php
/*
Plugin Name: Exoscale
Description: Exoscale Compute API
Version: 1.0.0
Author: Muhammad Atiq
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
    exit;
}

define( 'EXOSCALE_PLUGIN_NAME', 'EXOSCALE' );
define( 'EXOSCALE_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'EXOSCALE_PLUGIN_URL', plugin_dir_url(__FILE__) );
define( 'EXOSCALE_SITE_BASE_URL',  rtrim(get_bloginfo('url'),"/")."/");

require_once EXOSCALE_PLUGIN_PATH.'includes/main.php';